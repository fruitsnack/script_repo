# Scripts
This is a repo with scripts I wrote for my own usage.  Scripts are organized
in git submodules and this repo includes a script to populate a selected
directory (for example one of dirs in your PATH) with symlinks to executable
files from submodules.

## Usage:

Consult --help text of `populate` script.  
WARNING: be extremely cautious when using `-c` option as it will remove ANY
files in the specified path.

Example:  
`populate -i ~/Software/Scripts`

## License
Scripts in submodules are licensed under their respective licenses.  
`populate` script is licensed under coffeware (fork of beerware) license.
